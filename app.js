   
   
    var WIDTH = 1000 , HEIGHT = 600 , pi =Math.PI;                                          // Variables for Game size
    var canvas, ctx, keystate;                                                              // Variables for canvas , context , eventlistener 
    var player, ai, ball , score;                                                           // Variables for playeer , AI , and Ball
    var Uparrow = 38 ; DownArrow = 40;                                                      // Variables for Uparrow speed and DownArrow Speed
    
    player = {                                                                              // Player Method
        
        x:null,                                                                 
        y:null,
        width: 20 ,
        height: 100,
        speed: 8,                                                               
        
        update: function () {                                                               // update function for player

            if (keystate[Uparrow]) this.y -= this.speed ;
            if (keystate[DownArrow]) this.y += this.speed ;

        },
        draw: function () {                                                                 // draw function context for player
            ctx.fillRect(this.x, this.y , this.width , this.height); 
        }
    }

    ai = {                                                                                  // AI method
        
        x:null,
        y:null, 
        width: 20 ,
        height: 100,
        speed : 0.5,
        
        update: function () {                                                               // Update function for AI

            var desty = ball.y - ( this.height - ball.side )*0.9
            this.y += (desty - this.y ) * this.speed;

        },
        draw: function () {                                                                 // Draw function for AI
            ctx.fillRect(this.x, this.y , this.width , this.height); 
        }
    }

    ball = {                                                                                // Ball method
        x:null,
        y:null,
        side: 19,
        speed : 14,
        

        serve: function (side) {                                                            // serve function (randomize)
            
            
            var r = Math.random ();
            this. x = side === 1  ? player.x : ai.x - this.side;
            this. y = (HEIGHT - this.side) * r;

            var phi = 0.1 * pi * (1 - 2 *r );
            this.vel = {

                x : side * this.speed * Math.cos(phi),
                y : this.speed * Math.sin (phi)



            }




        },     
        update: function () {                                                               //update fun

            ball.x += this.vel.x;
            this.y += this.vel.y;


            if ( 0 > this.y ||  this.y + this.side > HEIGHT){

                var offset = this.vel.y < 0 ? 0 - this.y : HEIGHT - (this.y+this.side);
                this.y += 2*offset ;

                this.vel.y *= -1 ;
            }
            var AABBIntersect = function (ax , ay , aw , ah , bx , by , bw , bh ){

                return ax < bx+bw && ay < by + bh && bx  < ax + aw && by < ay + ah ;

            };

            var pdle =  this.vel.x < 0 ? player : ai ;
            if(AABBIntersect(pdle.x , pdle.y , pdle.width , pdle.height ,  
                    this.x ,this.y , this.side , this.side )
            
            ){
                this.x = pdle === player ? player.x + player.width : ai.x - this.side;
                var n = ( this.y + this.side - pdle.y)/(pdle.height + this.side);
                var phi = 0.25*pi*(2*n - 1 );

                var smash = Math.abs (phi) > 0.2 * pi ? 1.5 : 1 ;

                this.vel.x = smash * (pdle === player ? 1 : -1)*this.speed*Math.cos(phi);
                this.vel.y = smash * this.speed*Math.sin(phi); 
            }

                if ( 0 > this.x + this.side || this.x > WIDTH){
                 this.serve (pdle == player ? 1 : -1 );

                }

        },
            

        draw: function () {
            ctx.fillRect(this.x, this.y , this.side , this.side); 
        }
        
    }
    

    function main (){

        canvas = document.createElement('canvas');
        canvas.width = WIDTH;
        canvas.height = HEIGHT;
        ctx = canvas.getContext("2d");
        document.body.appendChild(canvas);
        

        keystate = {}
        document.addEventListener("keydown" , function(evt){
                keystate[evt.keyCode] =  true ;

        });
        document.addEventListener("keyup" , function(evt){
                delete keystate[evt.keyCode];

        });


        init ();


        var loop = function (){
            update ();
            draw();


            window.requestAnimationFrame(loop , canvas);
        };
            window.requestAnimationFrame(loop , canvas);

    }

    function updateScore() {
        ctx.fillStlye = "white";
        ctx.font = "16px Arial, sans-serif";
        ctx.textAlign = "left";
        ctx.textBaseline = "top";
        ctx.fillText("Score: " + points, 20, 20 );


        init ();


    }

    function init () {
        player.x = player.width;
        player.y = (HEIGHT - player.height)/2;
        
            ai.x = WIDTH - (player.width + ai.width);
            ai.y = (HEIGHT - ai.height)/2;

            ball.serve (1);
        }
    

    function update () {
        
        ball.update();
        player.update();
        ai.update();
        


    }

    function draw () {
        
        ctx.save();

        ctx.fillRect(0 , 0 , WIDTH , HEIGHT);
        
        ctx.fillStyle = '#2ff';

        ball.draw();
        player.draw();
        ai.draw();
       

        var w = 4 ;
        var x = (WIDTH - w ) * 0.5 ;
        var y = 0 ;
        var step = HEIGHT /20;
        
        while(y < HEIGHT){

            ctx.fillRect(x , y + step * 0.25 , w , step * 0.5);
            y += step;
        }

        ctx.restore();
        

    };



    main ();